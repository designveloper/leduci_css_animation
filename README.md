### CSS ANIMATION  

1. CSS Transitions and Transforms:  
```  
  **CSS Transform**  
    - change the shape and position of the affected content by modifying the coordinate space.  
    - do not disrupt the normal document flow.  
    - syntax:  
      transfrom
        affected by other properties
    - using perspective in parent + transform moving 2d to 3d space    
  **CSS Transitions**  
```  
2. Understanding CSS Animation:  
```  
  **Animation basic**
    - Two steps to a CSS Animation:  
      + Define the animation  
      + Assign it to a specific element (or elements)
    - The @keyframe Block:  
      + keyframes are a list describing what should happen over the course of the animation  
      
    Note:  
      + animation-name: name of keyframe  
      + animation-duration: 
      + animation-time-function  
      + animation-interation-count: how many animation repeats
  **Use animation-delay and animation-fill-mode**  
    - animation-delay: the time waiting next animation occurs  
    - animation-fill-mode  
      + backwards: applies styles from the first executed keyframe here  
      + forwards: applies styles from the last executed keyframe here 
      + both: apllies styles both before start of the animation and after the end of the animation  
  **Use animation-direction**  
    - properties: normal | reverse | alternate | alternate-reverse  
      + normal:  
        all iterations of the animation are played as specified.  
        keyframes play from start to end.  
      + reverse: 
        all iterations of the animation are played in the reverse direction 
        keyframes play end to start  
      + alternate: 
        the anmation direction is alternated witch each iteration of the animation  
        keyframes play from start to end, then end to start and continute alternating.
      + alternate-reverse:  
        the anmation direction is alternated witch each iteration of the animation  
        keyframes play from end to start, then start to end and continute alternating.
    **Timing function and easing**  
      - animation-timing-function  
        + predefined keywords  
          ease  
          linear  
          ease-in  
          ease-out  
          ease-in-out  
        + cubic-bezier functions 
          cubic-bezier(x1,y2,x1,y2)
        + steps  
```  
3. CSS Animation Building Blocks:  
```  
  **Infiitely looping animation**  
    - animation-interation-count: infinte;
  **Animate an element into place**  
    - using translateX, translateY, translate  
    - add some keyframes in animation with comma  
  **Pause and play with animation-play-state**  
    - animation-play-state: paused | running  
      => to pause or play the animation  
  **Animation 3D transforms**  
    - pespective
    - transform-origin
  **Animate sprite images**  
    - put the chain of sprite in 1 png
    - using steps for animation-timing-function
  **Chain multiple animations**  
    - using comma to using chain mutiple animation
```  
4. Applying CSS Animations to SVG:  
```  
  **Animate SVG with CSS**  
    - SVG fill  
    - Opacity  
    - CSS tranforms on SVG elements  
    NOTE:  
      - HTML elements default to a transform origin at their middle point (50%, 50%)  
      - SVG elements default to a transform origin at the top left of the SVG canvas  
```  
5. Performance, Browser Support, and Fallbacks:
```  
  **When to use CSS Animation**  
    - CSS keyframe:  
      + demonstrations or linear animations  
      + very simple state-based animaton  
    - CSS transitions:
      + Toggling between two states  
      + Animating on :hover, :active, :checked  
  **When to use Javascript**  
    - complex state animations  
    - dynamic animations  
    - physics  
    - support older brwosers  
  **High-performance animation**  
    - Most performant properties:  
      + opacity  
      + scale, rotation and position with transform  
    => Style > Layout > Paint > Composite
```  
